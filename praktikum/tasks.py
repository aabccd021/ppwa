import datetime
import os

HEROKU_API_KEY = os.environ.get('HEROKU_API_KEY')

attempt = 6

import atexit

from apscheduler.schedulers.background import BackgroundScheduler


def start_schedule():
    scheduler = BackgroundScheduler()
    scheduler.add_job(maintain_aren, 'interval', minutes=10)
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())


def maintain_aren():
    import heroku3
    import requests
    print("start maintaining aren " + str(datetime.datetime.now()))

    json = {'code': testCode, 'name': "TP2"}

    counter = 0

    for i in range(attempt):
        try:
            res = requests.post('http://aren-api.herokuapp.com/grader', json=json)
            print(res)
            if res.status_code != 200:
                counter += 1
        except requests.exceptions.RequestException as e:
            print(e)
            counter += 1

    print("error count" + str(counter))

    if counter >= attempt:
        heroku_conn = heroku3.from_key(HEROKU_API_KEY)
        app = heroku_conn.apps()['aren-api']
        app.kill_dyno('web')
        print("restarting aren...")
    else:
        print("aren is fine")


testCode = """
    import java.util.Scanner;

    public class SDA18192T {

        static Node theNode;
        static int M;

        public static void main(String args[]) {
            Scanner input = new Scanner(System.in);

            int N = input.nextInt();
            M = input.nextInt();

            int counter = 1;
            int acounter = N;
            Node prevNode = null;
            Node firstNode = null;
            Node checkPoint = null;
            Node currentNode = null;
            boolean done = false;
            int co = 0;

            for (int i = 1; i <= N; i++) {

                int K = input.nextInt();
                currentNode = new Node(K, prevNode, null);

                    if (prevNode == null) {
                        firstNode = currentNode;
                    } else {
                        prevNode.next = currentNode;

                        if (currentNode.value == prevNode.value) {
                            counter++;
                        } else {
                            if (counter >= 3 && checkPoint != null && !done) {
                                System.out.println("a");
                                System.out.println(i);
                                System.out.println(currentNode.value);
                                System.out.println(checkPoint.value);
                                checkPoint.next = currentNode;
                                currentNode.prev = checkPoint;
                                done = true;
                                co = counter;
                            }
                            checkPoint = prevNode;
                            counter = 1;
                        }

                    }
                prevNode = currentNode;
            }
            acounter -= co;
            firstNode.prev = currentNode;
            currentNode.next = firstNode;
            theNode = firstNode;

            ///
    //        System.out.println();
            System.out.println(theNode.value);
            System.out.println(theNode.next.value);
            System.out.println(theNode.next.next.value);
            System.out.println(theNode.next.next.next.value);
            printall(theNode);
            System.out.println(acounter);


            acounter -= cut(theNode);
            acounter -= cut(theNode.prev);

            ///
            printall(theNode);
            System.out.println(acounter);


            if (done(acounter)) {
                return;
            }

            int S = input.nextInt();
            for (int i = 0; i < S; i++) {
                int a = input.nextInt();
                int b = input.nextInt();
                acounter++;


    //            System.out.println("===");
    //            System.out.println(a+" "+b);

                acounter -= insertAndCut(a, b);

                //
                printall(theNode);
                System.out.println("counter" + acounter);

                if (done(acounter)) {
                    return;
                }
            }
            System.out.println(acounter);
        }

        static boolean done(int acounter) {
            System.out.println("kauntah" + acounter);
            if (acounter < 4) {
                System.out.println("MENANG");
                return true;
            }
            if (acounter > M) {
                System.out.println("KALAH");
                return true;
            }
            return false;
        }


        static int insertAndCut(int index, int value) {
            Node wantedNode = theNode;
            if(index==0){
                wantedNode = theNode.prev;
            }else {
                for (int i = 2; i <= index; i++) {
                    wantedNode = wantedNode.next;
                }
            }
    //        System.out.println("wanted"+wantedNode.value);
    //        System.out.println("wanted"+wantedNode.next.value);
    //        System.out.println("wanted"+wantedNode.next.next.value);
    //        System.out.println("wanted"+wantedNode.next.next.next.value);
            Node newNode = new Node(value, wantedNode, wantedNode.next);
            if(index==0){
                theNode = newNode;
            }
            wantedNode.next.prev = newNode;
            wantedNode.next = newNode;
    //        System.out.println("aaaa");
    //        printall(theNode);
    //        System.out.println("aaaa");
            return cut(newNode);
        }

        static int cut(Node anode) {
            if (anode == anode.next || anode == anode.next.next){
    //            System.out.println("masuka");
                return 0;
            }
            int counter = 1;
            boolean detected = false;

            if (anode == theNode) {
    //            System.out.println("masukb");
                detected = true;
            }

            Node currentNode = anode.next;
            Node rightNode = anode.next;
            while (currentNode.value == currentNode.prev.value) {
                counter++;
                if (currentNode == theNode || currentNode == theNode.prev) {
                    detected = true;
                }
                if (currentNode.next == anode.next) {
                    return counter;
                }
                currentNode = currentNode.next;
                rightNode = currentNode;
            }

            currentNode = anode.prev;
            Node leftNode = anode.prev;
            while (currentNode.value == currentNode.next.value) {
                counter++;
                if (currentNode == theNode || currentNode == theNode.prev) {
                    detected = true;
                }

                currentNode = currentNode.prev;
                leftNode = currentNode;
            }

            if (counter >= 3) {
    //            System.out.println(leftNode.value);
    //            System.out.println(leftNode.next.value);
    //            System.out.println(leftNode.next.next.value);
    //            System.out.println(leftNode.next.next.next.value);
    //            System.out.println(rightNode.value);
    //            System.out.println(rightNode.next.value);
    //            System.out.println(rightNode.next.next.value);
    //            System.out.println(rightNode.next.next.next.value);
                leftNode.next = rightNode;
                rightNode.prev = leftNode;
                if (detected) {
                    theNode = rightNode;
                }
    //            else {
    //
    //
    //                theNode = leftNode;
    //            }
                int a = cut(rightNode);
                return counter + a;
            }
            return 0;
        }

        static void printall(Node firstNode) {
            System.out.println("printing..");
            Node currentNode = firstNode;
            System.out.print(currentNode.value + " ");
            while (currentNode.next != firstNode) {
                currentNode = currentNode.next;
                System.out.print(currentNode.value + " ");
            }
            System.out.println();
        }
    }

    class Node {
        int value;
        Node prev;
        Node next;

        public Node(int value, Node prev, Node next) {
            this.value = value;
            this.prev = prev;
            this.next = next;
        }
    }

               """

if __name__ == '__main__':
    maintain_aren()
