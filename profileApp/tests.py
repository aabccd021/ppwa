from django.test import TestCase, Client
# Create your tests here.
from django.urls import resolve

from profileApp.views import buku, registrasi, hobby, jadwal


class ProfileAppUnitTest(TestCase):

    def test_registrasi(self):
        response = Client().get('/registrasi/')
        self.assertEqual(response.status_code, 200)

    def test_story10_using_registrasi(self):
        found = resolve('/registrasi/')
        self.assertEqual(found.func, registrasi)

    def test_hobby_func(self):
        found = resolve('/hobby/')
        self.assertEqual(found.func, hobby)

    def test_jadwal_func(self):
        found = resolve('/jadwal/')
        self.assertEqual(found.func, jadwal)

    def test_story10_using_registrasi_template(self):
        response = Client().get('/registrasi/')
        self.assertTemplateUsed(response, 'profileApp/registrasi.html')

    def test_story10_render_the_result(self):
        test = 'Registrasi'
        response = Client().get('/registrasi/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_buku_url_is_exist(self):
        response = Client().get('/buku/')
        self.assertEqual(response.status_code, 200)

    def test_story9_using_buku_func(self):
        found = resolve('/buku/')
        self.assertEqual(found.func, buku)

    def test_story9_using_buku_template(self):
        response = Client().get('/buku/')
        self.assertTemplateUsed(response, 'profileApp/buku.html')

    def test_story9_render_the_result(self):
        test = 'Buku'
        response = Client().get('/buku/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
