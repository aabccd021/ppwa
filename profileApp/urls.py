from django.urls import path

from . import views

urlpatterns = [
    path('hobby/', views.hobby, name='hobby'),
    path('register/', views.register, name='register'),
    path('jadwal/', views.jadwal, name='jadwal'),
    path('jadwal-baru/', views.new_schedule, name='new-schedule'),
    path('berhasil-jadwal/', views.schedule_post, name='success'),
    path('hapus-jadwal/', views.schedule_delete, name='schedule-delete'),
    path('buku/', views.buku, name='buku'),
    path('buku-json/', views.return_book_json, name='buku-json'),
    path('cari-buku-json/', views.book_search_json, name='cari-buku-json'),
    path('registrasi/', views.registrasi, name='registrasi'),
    path('registrasi-webservice/', views.registrasi_webservice, name='registrasi-webservice'),
    path('cek-email-webservice/', views.cek_email, name='cek-email-webservice'),
    path('apippw/mydiary/new', views.new_diary, name='new-diary'),
    path('apippw/mydiary/list', views.diary_list, name='diary-list'),
    path('restart-aren', views.restart_aren, name='restart-aren'),
    path('list-subscriber', views.list_subscriber, name='list-subscriber'),
    path('validasi-password', views.validasi_password, name='validasi-password'),
    path('delete-subscriber', views.delete_subscriber, name='delete-subscriber'),
    path('', views.index, name='index'),
]
