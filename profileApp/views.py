import json

import requests
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

from profileApp.models import Schedule, Regs, Diary
from .forms import JadwalForm

response = {}


def new_diary(request):
    body = json.loads(request.body)
    tgl = body['tgl']
    catatan = body['catatan']
    c = Diary(tgl=tgl, catatan=catatan)
    c.save()
    return HttpResponse()


def diary_list(request):
    d = Diary.objects.all()
    c = []
    for a in d:
        k = {"tgl": a.tgl, "catatan": a.catatan}
        c.append(k)
    return JsonResponse({"ba": c})


def restart_aren(request):
    import heroku3
    import os
    print("restart aren request")
    heroku_conn = heroku3.from_key(os.getenv('HEROKU_API_KEY'))
    app = heroku_conn.apps()['aren-api']
    app.kill_dyno('web.1')
    print("restarting aren")
    return HttpResponse()


def return_book_json(request):
    res = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    mimetype = 'application/json'
    return HttpResponse(res, mimetype)


def book_search_json(request):
    print(str(request.body))
    print(type(request.body))
    # data = json.dumps(request.body)
    name = (str(request.body))[7:]
    name = name[:-1]
    print(name)
    url = "https://www.googleapis.com/books/v1/volumes?q=" + name
    res = requests.get(url)
    print(res)
    mimetype = 'application/json'
    return HttpResponse(res, mimetype)


def registrasi(request):
    return render(request, 'profileApp/registrasi.html')


def cek_email(request):
    body = json.loads(request.body)
    email = body['email']
    print(email)
    print("aaaa")

    email_exist = False
    print()
    if Regs.objects.filter(email=email).exists():
        email_exist = True
    print("aab")
    return JsonResponse({'email_exist': email_exist})


def registrasi_webservice(request):
    body = json.loads(request.body)
    email = body['email']
    name = body['name']
    password = body['password']
    newreg = Regs(
        email=email,
        name=name,
        password=password)
    newreg.save()
    return HttpResponse()


def list_subscriber(request):
    sub_list = []
    for a in Regs.objects.all():
        sub_list.append({"nama": a.name, "email": a.email})
        print(a.name)
    return JsonResponse({'sub_list': sub_list})


def validasi_password(request):
    body = json.loads(request.body)
    email = body['email']
    print(email)
    password = body['password']
    a = Regs.objects.get(email=email)
    if a.password == password:
        print("password benar")
        return JsonResponse({'passwordIsValid': True})
    print("password salah")
    return JsonResponse({'passwordIsValid': False})


def delete_subscriber(request):
    body = json.loads(request.body)
    email = body['email']
    try:
        Regs.objects.get(email=email).delete()
        return JsonResponse({'deleteSuccess': True})
    except:
        return JsonResponse({'deleteSuccess': False})


def buku(request):
    return render(request, 'profileApp/buku.html')


def index(request):
    return render(request, 'profileApp/index.html', {})


def hobby(request):
    return render(request, 'profileApp/hobby.html', {})


def register(request):
    return render(request, 'profileApp/buku_tamu.html')


def new_schedule(request):
    form = JadwalForm()
    return render(request, 'profileApp/jadwal_baru.html', {'form': form})


def schedule_post(request):
    form = JadwalForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        response['hari'] = request.POST['hari']
        response['tanggal'] = request.POST['tanggal']
        response['jam'] = request.POST['jam']
        response['nama'] = request.POST['nama']
        response['tempat'] = request.POST['tempat']
        response['kategori'] = request.POST['kategori']
        schedule = Schedule(hari=response['hari'],
                            tanggal=response['tanggal'],
                            jam=response['jam'],
                            nama=response['nama'],
                            tempat=response['tempat'],
                            kategori=response['kategori'])
        schedule.save()
        html = 'profileApp/jadwal_berhasil.html'
        return render(request, html, response)
    else:
        return render(request, 'profileApp/standar.html', {'text': 'input tidak valid'})


def jadwal(request):
    jadwals = Schedule.objects.all().values()
    response['jadwals'] = jadwals
    return render(request, 'profileApp/jadwal.html', response)


def schedule_delete(request):
    Schedule.objects.all().delete()
    return render(request, 'profileApp/standar.html', {'text': 'semua jadwal berhasil dihapus'})
