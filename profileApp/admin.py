from django.contrib import admin
from .models import Schedule, Regs

admin.site.register(Schedule)
admin.site.register(Regs)

# Register your models here.

